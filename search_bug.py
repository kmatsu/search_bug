import re
class SearchBug:
    bug_text_path = "bug.txt"
    landscape_text_path = "landscape.txt"
    bug_pattern = []
    landscapes = []
    total_occurance = 0
    
    def __init__(self):
        f = open(self.bug_text_path, 'r')
        for line in f.readlines():
            self.bug_pattern.append(line.rstrip())
        f.close()

        f = open(self.landscape_text_path, 'r')
        self.landscapes = f.readlines()
        f.close()

    def search_occurance(self):
        for index, landscape in enumerate(self.landscapes):
            if (index + len(self.bug_pattern)) > len(self.landscapes):
                break
            for match in re.finditer(re.escape(self.bug_pattern[0]), landscape):
                for index_below in range(1, len(self.bug_pattern)):
                    if self.bug_pattern[index_below] != self.landscapes[index + index_below][match.start():match.start()+len(self.bug_pattern[index_below])]:
                        break
                else:
                    self.total_occurance += 1

    def reult_output(self):
        print("bug occurance: {}".format(self.total_occurance))

sb = SearchBug()
sb.search_occurance()
sb.reult_output()
